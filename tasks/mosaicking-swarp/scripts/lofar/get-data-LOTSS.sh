#!/bin/bash
# A script to download some LOFAR survey fields.
# Assuming you have retrieved a file list (.csv) from the LOFAR surveys website

# get list of file locations from an input csv file provided as an argument
grep "http" scripts/lofar/lofar-files.csv | cut -c-51 > scripts/lofar/lofar-files.txt
# download files in parallel
echo "downloading "$(wc -l < scripts/lofar/lofar-files.txt)" files..."

cat scripts/lofar/lofar-files.txt | xargs -P 20 -I{} wget -nc -q -P data {}

# rename files to have .fits
cd data
ls P* | while read line; do mv $line $line".fits"; done
cd ../