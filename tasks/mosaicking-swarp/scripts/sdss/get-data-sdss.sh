#!/bin/bash
# this script will search for and grab the fits file list from the main script, and download them in parallel.
# it requires the J*.sh file as an argument from the SDSS mosaic generation server to grab the list of requires fields.

# get list of files
grep "https" scripts/sdss/J011700.00+280000.0.small.sh | cut -c 17- > scripts/sdss/sdss-files.txt

# download files in parallel
echo "downloading "$(wc -l < scripts/sdss/sdss-files.txt)" files..."
cat scripts/sdss/sdss-files.txt | xargs -P 20 -I{} wget -nc -q -P data {}

# decompress files in parallel
echo "decompressing files..."
grep "https" scripts/sdss/J011700.00+280000.0.small.sh | cut -c 84- > scripts/sdss/bz2-files.txt
cd data
cat ../scripts/sdss/bz2-files.txt | xargs -P 20 -I{} bzip2 -d -k {}
rm *.bz2
cd ../