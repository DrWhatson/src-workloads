#!/bin/bash

echo "making SDSS mosaic..."

# /scripts is mounted inside the container as an absolute path.
/scripts/sdss/J011700.00+280000.0.small.sh # swarp parameters for SDSS images built into this run script
