{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "dfec4994-8d70-41a7-91db-d47b75a4a379",
   "metadata": {},
   "source": [
    "# Image Mosaicking with SWARP\n",
    "\n",
    "This notebook demonstrates the process of mosaicking several images together using the SWARP tool.\n",
    "\n",
    "By default data are retrieved from the Rucio data lake prototype, with the user needing to authenticate using their SKA IAM credentials.\n",
    "\n",
    "## Log in to SRCNet\n",
    "\n",
    "Instantiate SRCNet Astroquery client, and log in at the SKA IAM service. User will need to report the generated device code to the IAM service when prompted to authenticate this client."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c6a90efb-ecca-4827-871b-e04f6f630338",
   "metadata": {},
   "outputs": [],
   "source": [
    "from astroquery.srcnet import SRCNet\n",
    "srcnet=SRCNet()\n",
    "srcnet.login()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ba9fe62b-381e-4a40-9d6a-e36ad45e6900",
   "metadata": {},
   "source": [
    "## Discover data and display in a table\n",
    "\n",
    "Here we use the `query_region` function to discover all files within 0.5 deg of the field we are looking at. We only use files that are in the 'sdss' `obs_collection`. We can display these results by placing them in an `astropy.table.Table`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5723ba36-c929-45f6-a4c1-a803f4e2628c",
   "metadata": {},
   "outputs": [],
   "source": [
    "from astropy.table import Table\n",
    "\n",
    "region_results = srcnet.query_region(coordinates='19.25deg 28.0deg', radius=0.5)\n",
    "sdss_results = [result for result in region_results if result.get(\"obs_collection\") == \"sdss\"]\n",
    "\n",
    "if len(sdss_results) == 0:\n",
    "    raise Exception(\"No results found!\")\n",
    "\n",
    "display_columns = ['obs_id', 's_ra', 's_dec', 'dist', 'obs_collection', 'access_url']\n",
    "data = []\n",
    "for record in sdss_results:\n",
    "    column_data = [record[column_name] for column_name in display_columns]\n",
    "    data.append(column_data)\n",
    "\n",
    "# Create an Astropy Table with the extracted columns\n",
    "table = Table(rows=data, names=display_columns)\n",
    "table"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c0c59090-9db7-4481-aebf-807196adc8ba",
   "metadata": {},
   "source": [
    "## Option 1: Download data sequentially\n",
    "\n",
    "This cell will loop over the SDSS files discovered in the last step and use the `srcnet.get_data` method to download the file locally.\n",
    "\n",
    "Use the following cell ('Option 2') to download files in parallel (which is faster)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9c6e57a6-9963-47d3-87f4-8991907ab396",
   "metadata": {},
   "outputs": [],
   "source": [
    "import warnings\n",
    "import time\n",
    "\n",
    "downloaded_files = []\n",
    "\n",
    "start_time = time.time()\n",
    "for result in sdss_results:\n",
    "    scope, name = result[\"obs_id\"].split(\":\")\n",
    "    try:\n",
    "        data = srcnet.get_data(namespace=scope, name=name)\n",
    "        downloaded_files.append(name)\n",
    "    except Exception as e:\n",
    "        warnings.warn(f\"Failed to retrieve data for result: {result['obs_id']}. Error: {str(e)}\")\n",
    "        continue\n",
    "\n",
    "execution_time = time.time() - start_time\n",
    "print(f\"Downloaded {len(downloaded_files)} files in {execution_time:.1f} s\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "85c35b4d-c000-42ac-9be4-063ab1f703fd",
   "metadata": {},
   "source": [
    "## Option 2: Download data in parallel\n",
    "\n",
    "Use a `ThreadPoolExecutor` to download files in parallel. If there are issues, use the sequential download cell above instead."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a6ebce8e-445f-4cb3-b871-b4a9a756d967",
   "metadata": {},
   "outputs": [],
   "source": [
    "from concurrent.futures import ThreadPoolExecutor\n",
    "import logging\n",
    "import time\n",
    "\n",
    "logger = logging.getLogger(\"astropy\")\n",
    "logger.setLevel(logging.CRITICAL)\n",
    "\n",
    "def get_data(result):\n",
    "    scope, name = result[\"obs_id\"].split(\":\")\n",
    "    srcnet.get_data(namespace=scope, name=name)\n",
    "    return name\n",
    "\n",
    "# Define the maximum number of threads to use\n",
    "max_threads = 4\n",
    "\n",
    "start_time = time.time()\n",
    "# Create a ThreadPoolExecutor\n",
    "with ThreadPoolExecutor(max_workers=max_threads) as executor:\n",
    "    # Submit the get_data function for each result in parallel\n",
    "    futures = [executor.submit(get_data, result) for result in sdss_results]\n",
    "\n",
    "    # Wait for all tasks to complete and collect the results\n",
    "    downloaded_files = [future.result() for future in futures]\n",
    "\n",
    "execution_time = time.time() - start_time\n",
    "print(f\"Downloaded {len(downloaded_files)} files in {execution_time:.1f} s\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3972ddfd-b827-44f3-9ad2-ee1496612213",
   "metadata": {},
   "source": [
    "## Run SWARP to produce image mosaic\n",
    "\n",
    "Run SWARP via a subprocess to produce an output mosaic. This can take >30min for more than 100 images."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "19e93b22-486c-4d02-a585-bad0ab59cf52",
   "metadata": {},
   "outputs": [],
   "source": [
    "import time\n",
    "import subprocess\n",
    "\n",
    "# Build SWARP CLI command, writing output to a file\n",
    "command = \"swarp {} -c etc/default.swarp > swarp_console.log 2>&1\".format(' '.join(downloaded_files))\n",
    "\n",
    "start_time = time.time()\n",
    "try:\n",
    "    output = subprocess.check_output(command, shell=True)\n",
    "    execution_time = time.time() - start_time\n",
    "    print(f\"SWARP run completed successfully in {execution_time:.1f} s\")\n",
    "except subprocess.CalledProcessError as e:\n",
    "    # Handle errors (non-zero exit code)\n",
    "    print(\"Command failed with exit code\", e.returncode)\n",
    "    print(\"Error output:\", e.output)\n",
    "except Exception as e:\n",
    "    # Handle other exceptions\n",
    "    print(\"An error occurred:\", e)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "763ce2f3-d4fe-4589-9d76-5c7ca782204a",
   "metadata": {},
   "source": [
    "## Plot results"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4280e220-071f-4e47-975e-a79ce8c0d45b",
   "metadata": {},
   "outputs": [],
   "source": [
    "from astropy.wcs import WCS\n",
    "from astropy.io import fits\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "image_file = ('/home/jovyan/scripts/sdss/J011700.00+280000.0-r.fits')\n",
    "hdu = fits.open(image_file)[0]\n",
    "wcs = WCS(hdu.header)\n",
    "\n",
    "plt.subplot(projection=wcs) \n",
    "plt.imshow(hdu.data, origin='lower', vmin=-0.001, vmax=0.05)\n",
    "plt.grid(color='white', ls='solid')\n",
    "plt.show()"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
