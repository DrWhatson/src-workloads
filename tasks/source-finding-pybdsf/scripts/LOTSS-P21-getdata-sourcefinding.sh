#!/bin/bash
# this script downloads data, and runs a python source-fiding script.

# download to the /data folder since entrypoint is /data
wget -nc --cut-dirs=5 -P data https://vo.astron.nl/getproduct/hetdex/data/low-mosaics/P21-low-mosaic.fits

# run the source-finding (entrypoint is /data)
python3 ../scripts/source-finding.py ./P21-low-mosaic.fits
