# this must be run on the canfar platform where rucio storage is attached

import bdsf
import sys
from astropy.io import fits
import os

def do_sourcefinding(imagename):
    # get beam info manually. If fits headers are improperly formatted you may need to set some paramters
    # (simulated SKA image seems to cause PyBDSF issues finding this info)
    #hdu = fits.open(imagepath, mode='update')
    #beam_maj = hdu[0].header['BMAJ']
    #beam_min = hdu[0].header['BMIN']
    #beam_pa = hdu[0].header['BPA'] # not in SKA fits header, but we know it's circular
    #beam_pa = 0
    # set rms_box as 30 beams in pixels
    #pixperbeam = beam_maj/hdu[0].header['CDELT2']
    #print(pixperbeam)
    #hdu.close()
    # Run sourcefinding using some sensible hyper-parameters. PSF_vary and adaptive_rms_box is more computationally intensive, off for now
    img = bdsf.process_image(imagename, adaptive_rms_box=False, advanced_opts=True,\
        atrous_do=False, psf_vary_do=False, psf_snrcut=5.0, psf_snrcutstack=10.0,\
        output_opts=True, output_all=True, opdir_overwrite='append', frequency=120000000,\
        blank_limit=None, thresh='hard', thresh_isl=4.0, thresh_pix=5.0, psf_snrtop=0.30,\
        rms_map=False, do_cache=False, quiet=False) 
    # can save the img object as a pickle file, so we can do interactive checks after pybdsf has run
    # turns out this doesn't work you have to run it inside an interactive python session
    # save_obj(img, 'pybdsf_processimage_'+imagename[:-5])


# set the location of files on the rucio server. Unfortunately this has to be done by hand right now since there is no way to search for their location since we didn't set metadata yet. 

# list of file paths from the attached rucio storage
files = ["/data/rse_data/test_rse/dev/deterministic/src-workloads/00/8f/P001d41-LOTSS-DR2-10jan24", 
             "/data/rse_data/test_rse/dev/deterministic/src-workloads/03/22/P011d41-LOTSS-DR2-10jan24",
             "/data/rse_data/test_rse/dev/deterministic/src-workloads/04/74/P000d36-LOTSS-DR2-10jan24",
             "/data/rse_data/test_rse/dev/deterministic/src-workloads/1c/03/P003d36-LOTSS-DR2-10jan24",
             "/data/rse_data/test_rse/dev/deterministic/src-workloads/3e/8f/P009d36-LOTSS-DR2-10jan24",
             "/data/rse_data/test_rse/dev/deterministic/src-workloads/4e/82/P004d41-LOTSS-DR2-10jan24",
             "/data/rse_data/test_rse/dev/deterministic/src-workloads/61/d5/P000d38-LOTSS-DR2-10jan24",
             "/data/rse_data/test_rse/dev/deterministic/src-workloads/6e/a3/P012d36-LOTSS-DR2-10jan24",
             "/data/rse_data/test_rse/dev/deterministic/src-workloads/7d/a6/P007d39-LOTSS-DR2-10jan24",
             "/data/rse_data/test_rse/dev/deterministic/src-workloads/b2/92/P004d38-LOTSS-DR2-10jan24",
             "/data/rse_data/test_rse/dev/deterministic/src-workloads/cd/e1/P006d36-LOTSS-DR2-10jan24",
             "/data/rse_data/test_rse/dev/deterministic/src-workloads/e2/8c/P010d39-LOTSS-DR2-10jan24",
             "/data/rse_data/test_rse/dev/deterministic/src-workloads/f1/29/P356d36-LOTSS-DR2-10jan24",
             "/data/rse_data/test_rse/dev/deterministic/src-workloads/fa/05/P007d41-LOTSS-DR2-10jan24",

imagenames = ["P001d41-LOTSS-DR2-10jan24", 
             "P011d41-LOTSS-DR2-10jan24",
             "P000d36-LOTSS-DR2-10jan24",
             "P003d36-LOTSS-DR2-10jan24",
             "P009d36-LOTSS-DR2-10jan24",
             "P004d41-LOTSS-DR2-10jan24",
             "P000d38-LOTSS-DR2-10jan24",
             "P012d36-LOTSS-DR2-10jan24",
             "P007d39-LOTSS-DR2-10jan24",
             "P004d38-LOTSS-DR2-10jan24",
             "P006d36-LOTSS-DR2-10jan24",
             "P010d39-LOTSS-DR2-10jan24",
             "P356d36-LOTSS-DR2-10jan24",
             "P007d41-LOTSS-DR2-10jan24",

# Create a symbolic link because pybdsf can't write to rucio storage and options to change input/output dirs don't work  in pybdsf.
for file in files:
    os.system('ln -s '+file)

#for file in files:
#    os.symlink(file)

for image in imagenames:
    do_sourcefinding(image)
