# Source-finding task using PyBDSF

This task will run the source-finder PyBDSF on a LOFAR image from the LoTSS DR2 survey.

## How to run the task

Clone this repository and add execute permissions to files in the repository

    chmod 755 -R *

We use a Makefile to organise different ways to run this task, depending on your need. The Makefile shows all the available options, but to run it fully automatically the following command will pull the latest container, download the data, and run the task:

    make run-full

You can also use the Makefile to run things separately step by step, such as pulling the image (`make pull-image`), downloading the data (`make get-data`), and running the source-finding (`make run-task`).

## How to use a Jupyter notebook

The Makefile also includes options for using Jupyter notebooks.

    make build-image-jupyter
    make run-local-jupyter

then a link will appear which you can copy into your browser to access. You can then start a terminal to run things with the Makefile, or you can load the Jupyter notebook in the scripts directory.

Finally, you can run carta to interactively explore the plot and source catalogue

    docker run -ti -p 3002:3002 -v $PWD:/images cartavis/carta:latest
