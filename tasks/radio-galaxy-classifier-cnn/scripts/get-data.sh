#!/bin/bash

wget -nc -P data http://www.jb.man.ac.uk/research/MiraBest/MiraBest_F/MiraBest_F_batches.tar.gz
cd ./data
tar -xvf MiraBest_F_batches.tar.gz
cd ../