#!/usr/bin/python3

# This runs a cnn classifier on the MiraBest radio galaxy dataset.
# It has been adapted by Alex Clarke using Fiona Porter's and Hongming Tang's code.

# The steps are:

# Load and normalizing the FRDEEP-F training and test datasets using torchvision
# Define a Convolutional Neural Network
# Define a loss function
# Train the network on the training data
# Test the network on the test data


import matplotlib.pyplot as plt
import numpy as np

import torch
import torchvision
import torchvision.transforms as transforms
from torchsummary import summary
import torch.nn as nn
import torch.nn.functional as F

import torch.optim as optim

# The next two lines depends what your class selection is. MirabBest_F uses9 classes, MBFRFULL uses binary FRI vs FRII.
#from MiraBest_F import MiraBest_F as MiraBest
from MiraBest_F import MBFRFull as MiraBest

torch.manual_seed(2809)
torch.cuda.manual_seed(2809)

transform = transforms.Compose(
    [transforms.ToTensor(),
     transforms.Normalize((0.0031,), (0.0350,))]) # mean and variance of dataset

#Load in the training and test datasets. The first time you do this it will download the data to your working directory, but once the data is there it will just use it without repeating the download.

trainset = MiraBest(root='./', train=True, download=True, transform=transform)
batch_size_train = 2
trainloader = torch.utils.data.DataLoader(trainset, batch_size=batch_size_train, shuffle=True, num_workers=0)
testset = MiraBest(root='./', train=False, download=True, transform=transform)
batch_size_test = 2
testloader = torch.utils.data.DataLoader(testset, batch_size=batch_size_test, shuffle=True, num_workers=0)

classes = ('FRI', 'FRII')

# this section isn't needed unless you want to run things interactively.
'''
def imshow(img):
    # unnormalize
    img = img #/ 2 + 0.5
    npimg = img.numpy()
    plt.imshow(np.transpose(npimg, (1, 2, 0)))
    #plt.imshow((img, (1, 2, 0)))
    plt.show()

# get some random training images
dataiter = iter(trainloader)
images, labels = next(dataiter)

# show images
imshow(torchvision.utils.make_grid(images))
# print labels
print(' '.join('%5s' % classes[labels[j]] for j in range(batch_size_train)))
'''

class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.conv1 = nn.Conv2d(1, 6, 5) # 1st=input/greycsale, 2nd=output/dimensions, 3rd=kernalsize, 4th=pad        
        self.pool = nn.MaxPool2d(2, 2)
        self.conv2 = nn.Conv2d(6, 16, 5)
        self.fc1 = nn.Linear(16 * 34 * 34, 120)
        self.fc2 = nn.Linear(120, 84)
        self.fc3 = nn.Linear(84, 10)

    def forward(self, x):
        # conv1 output width: input_width - (kernel_size - 1) => 150 - (5-1) = 146
        # pool 1 output width: int(input_width/2) => 73
        x = self.pool(F.relu(self.conv1(x)))
        # conv2 output width: input_width - (kernel_size - 1) => 73 - (5-1) = 69
        # pool 2 output width: int(input_width/2) => 34
        x = self.pool(F.relu(self.conv2(x)))
        x = x.view(-1, 16 * 34 * 34)
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = self.fc3(x)
        return x

net = Net()
# Move the network to the GPU
device = torch.device("cuda")
net = net.to(device)
summary(net,(1,150,150))

#We'll use Classification Cross-Entropy loss and Adagrad with momentum for optimization:
criterion = nn.CrossEntropyLoss()
optimizer = optim.Adagrad(net.parameters(), lr=0.01)

nepoch = 10  # number of epochs
print_num = 250
for epoch in range(nepoch):  # loop over the dataset multiple times

    running_loss = 0.0
    for i, data in enumerate(trainloader, 0):
        # get the inputs
        inputs, labels = data
        # move data to gpu device
        inputs, labels = inputs.to(device), labels.to(device)

        # zero the parameter gradients
        optimizer.zero_grad()

        # forward + backward + optimize
        outputs = net(inputs)
        loss = criterion(outputs, labels)
        loss.backward()
        optimizer.step()

        # print statistics
        running_loss += loss.item()
        if i % print_num == (print_num-1):    # print every 50 mini-batches
            print('[%d, %5d] loss: %.3f' %
                  (epoch + 1, i + 1, running_loss / print_num))
            running_loss = 0.0

print('Finished Training')

'''
dataiter = iter(testloader)
images, labels = next(dataiter)

# print images
imshow(torchvision.utils.make_grid(images))
print('GroundTruth: ', ' '.join('%5s' % classes[labels[j]] for j in range(batch_size_test)))

outputs = net(images)

_, predicted = torch.max(outputs, 1)
print('Predicted: ', ' '.join('%5s' % classes[predicted[j]] for j in range(batch_size_test)))
'''

correct = 0
total = 0
with torch.no_grad():
    for data in testloader:
        images, labels = data[0].to(device), data[1].to(device) # move data to gpu device
        outputs = net(images)
        _, predicted = torch.max(outputs.data, 1)
        total += labels.size(0)
        correct += (predicted == labels).sum().item()

print('Accuracy of the network on the ' + str(total) + ' test images: %d %%' % (100 * correct / total))
