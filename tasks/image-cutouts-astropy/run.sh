#!/bin/bash
# check to pull latest version, if needed
docker pull registry.gitlab.com/ska-telescope/src/src-workloads/image-cutouts-astropy:latest
# run container pipeline
docker run -it --rm --name image-cutouts-astropy -v "$(pwd)"/scripts:/scripts/ -v "$(pwd)"/examples:/examples/ registry.gitlab.com/ska-telescope/src/src-workloads/image-cutouts-astropy:latest
