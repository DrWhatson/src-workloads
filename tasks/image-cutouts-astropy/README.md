# Astro-Cutouts

## Description
A very simple python based client based workflow to extract cutouts from the Lofar Two-metre
 Sky Survey (LoTSS). Starting with a simple CSV list file of sources in the for
mat of id, ra, dec, size, the workflow will talk to the archive, find which ima
ge files are involved, download them and make the cutouts and store them as eit
her png and/or fits. Ideally this is for a smaller "region of interest" within a larger sky survey, where it makes sense to download part of the survey and do many cutouts

This is more an execise in generating and storing example workflows rather than
 the cutout service itself.

## Dependences

Requires numpy, astropy, pyvo, pandas, json, click, urllib.request, subprocess 

The exact requirements are given in the requirements.txt file 

## How to run the task

The run.sh script will pull the latest container from the registry (no building is required), and execute the task.

    run.sh

## Installation by hand
Clone the repository 

    git clone https://gitlab.com/DrWhatson/astro-cutouts.git

Make a new virtualenv if you want to test and/or keep seperate

    python3 -m venv .venv
    . .venv/bin/activate

Install with 

    pip install -r requirements.txt .


## Usage
The package uses click https://click.palletsprojects.com/en/8.1.x to provide a command line interface

After installation a cutouts command should be available 

    cutouts --help
    Usage: cutouts [OPTIONS] INPUT
    
    Process input source list to cutouts
    
    Options:
      --survey-dir TEXT  Directory for downloaded images
      --output-dir TEXT  Director for cutouts
      --format TEXT      Output format png or fits
      --vmin TEXT        min value for colourmap
      --vmax TEXT        max value for colourmap
      --cmap TEXT        matplotlib colour table to use
      --geometry TEXT    Array tile mxn for pngs. None for individual pngs
      --help             Show this message and exit.


### Source file format

The expected format for the input source list file is a csv file with first row 
"id, ra, dec, size"

id: An ascii string for the source eg TauA, 3C273, J1719−1438, although care needs to be taken as these may be used to generate filenames
ra: RA position in hours (format not too critical as astropy decodes most)
dec: Dec position in degrees
size: Size of field in arcminutes 

Below is an example

    id, ra, dec, size
    src1, 23:59:03.78, +22:54:19.98, 10.0
    src2, 23:56:39.44, +23:26:46.79, 10.0
    src3, 23:55:13.32, +23:40:15.64, 10.0
    src4, 00:01:39.49, +23:29:25.27, 10.0
    src5, 00:02:18.30, +24:44:27.40, 10.0
    src6, 00:06:09.47, +24:02:23.76, 10.0

### Format

The "--format" defines the output file type of the cutouts. 

"--format fits" will produce fits files of the cutouts with the filename being the source id appended with "_cutout.fits"

"--format png" or jpg or pdf ( or whatever matplotlib can produce), will make bitmap images files instead with "<source_id>_cutout.<format>"

Adding "--geometry nxm" option will collect those bitmaps into an array of images n columns and m rows in the choosen format, except for fits where it is ignored

The default format is fits files

### Bitmap options

--vmin Sets the minimum value for the bitmap colour scale. Defaults to the minimum of the cutout area

--vmax Sets the maximum value. Defaults to the maximum of the cutout

--cmap Sets the matplotlib colour map to use such as "gray" or "viridis". Defaults to "afmhot"

### Directories

--survey-dir tells the script where to store the intermediate mosaic images used to make the cutouts. If the script is run again this directory is checked so to avoid repeating the download. Default is the working directory

--output-dir tells the script where to store the output cutouts. Default is the working directory

## Containers

### Docker

A Dockerfile is included in the repo which can be built with 

    docker build -t <name/tag> .

Then the application can be run by using

    docker run -v "$PWD":/work  <name/tag> cutouts <args>

A docker image is stored in the gitlab container repository as 
https://gitlab.com/DrWhatson/image-cutouts-astropy/container_registry/4428599

### Singularity

A singularity def file "image_cutouts_singularity.def" is also included in the repo

    singularity build --fakeroot image_cutouts_singularity.sif image_cutouts_singularity.def

And this may be run with 

    singularity exec image_cutouts_singularity.sif cutouts <args>


## Examples

There is also an examples directory, which contains a directory of short source lists for different images selected by hand via ds9 inspection. Bash scripts for the different output options

### Fits

test_fits_out.sh

### Png/Jpeg/PDF/etc. 

test_png_out.sh
test_jpeg_out.sh
test_pdf_out.sh

### Array plots

test_png_array_out.sh
test_pdf_array_out.sh
