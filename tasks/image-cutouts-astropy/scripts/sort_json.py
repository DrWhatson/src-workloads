import json
import click
import pandas
from astropy.io import fits
from astropy import wcs
from astropy.nddata.utils import Cutout2D,NoOverlapError
from astropy import units as u
from astropy.coordinates import Angle
from astropy.coordinates import SkyCoord
import urllib.request
import matplotlib
matplotlib.use("Agg")
import pylab as plt
import numpy as np
import os, sys

@click.group()
def cli():
    pass


@click.command()
@click.option('--survey-dir',default='images',help='Directory for downloaded images')
@click.option('--output-dir',default='output',help='Director for cutouts')
@click.argument('source_list', type=click.Path(exists=True), )
@click.argument('input_json', type=click.Path(exists=True), )
def make_fits(source_list, input_json,survey_dir, output_dir):
    """Make directory of fits cutout images"""
 
    # Read JSON file
    images, data = _process_JSON(input_json)

    # Get sources for each image
    image_srcs = _find_image_sources(images, data, survey_dir)

    # Read in source list
    srcs = pandas.read_csv(source_list,index_col="id")

    # Made cutouts for each image
    for item in image_srcs:  
        image = item[0]
        print(f"Opening image file {item[0]}")

        # Open image fits file
        f1 = fits.open(image)
        data = f1[0].data
        header = f1[0].header
        w1 = wcs.WCS(header)
        pixel_scale = w1.pixel_scale_matrix[1, 1] * u.deg
        

        # Produce a fits file for each source
        for src in item[1]:
            cutout = _get_cutout(data, w1, srcs, src, pixel_scale)

            # Create a new header for the cutout
            cutout_header = cutout.wcs.to_header()

            # Create a new FITS HDU for the cutout data and header
            cutout_hdu = fits.PrimaryHDU(cutout.data, header=cutout_header)

            # Use id to generate output filename
            cutout_filename = f"{src}_cutout.fits"
            cutout_path = os.path.join(output_dir,cutout_filename)
            print(cutout_path)

            # Save the cutout FITS file
            cutout_hdu.writeto(cutout_path, overwrite=True)


def _process_JSON(input_json):

      # Open the JSON file
    with open(input_json, 'r') as infile:
        data = json.load(infile)

    # Find images again
    images = []
    for item in data:
        for field in item[1]:
            if field not in images:
                images.append(field)

    return images, data
  

def _find_image_sources(images, data, survey_dir):
    image_srcs = []
    # For each image find image files and list of sources
    for image in images:
        # Extract the filename from the URL
        filename = os.path.basename(image)
        filename = urllib.parse.unquote(filename)+'.fits'

        # Set the complete image path
        image_path = os.path.join(survey_dir, filename)

        srcs = []
        for item in data:
            if image in item[1]:
                srcs.append(item[0])
    
        image_srcs.append((image_path,srcs))

    return image_srcs


def _get_cutout(data, w1, srcs, src, pixel_scale):
    row = srcs.loc[src]  
    id = src
    ra = Angle(row[0],unit=u.hour)
    dec = Angle(row[1],unit=u.deg)
    pos = SkyCoord(ra,dec)
    siz = row[2] * u.arcmin # in arcmin
    cutout_size = int((siz / pixel_scale).decompose().value)
            
    cutout = Cutout2D(data, pos, (cutout_size,cutout_size), wcs=w1)

    return cutout


@click.command()
@click.option('--survey-dir',default='images',help='Directory for downloaded images')
@click.option('--output-dir',default='output',help='Directory for cutouts')
@click.option('--fmt',default='png',help='format for cutouts')
@click.option('--vmin',default='auto',help='min value for colourmap')
@click.option('--vmax',default='auto',help='max value for colourmap')
@click.option('--cmap',default='afmhot',help='matplotlib colour table to use')
@click.argument('source_list', type=click.Path(exists=True), )
@click.argument('input_json', type=click.Path(exists=True), )
def make_bitmaps(source_list, input_json,survey_dir, output_dir, fmt, vmin, vmax, cmap):
    """Make directory of bitmaps cutout images"""
 
    # Read JSON file
    images, data = _process_JSON(input_json)

    # Get sources for each image
    image_srcs = _find_image_sources(images, data, survey_dir)

    # Read in source list
    srcs = pandas.read_csv(source_list,index_col="id")

    # Made cutouts for each image
    for item in image_srcs:  
        image = item[0]
        print(f"Opening image file {item[0]}")

        # Open image fits file
        f1 = fits.open(image)
        data = f1[0].data
        header = f1[0].header
        w1 = wcs.WCS(header)
        pixel_scale = w1.pixel_scale_matrix[1, 1] * u.deg
        

        # Produce a file for each source
        for src in item[1]:
            cutout = _get_cutout(data, w1, srcs, src, pixel_scale)

            if vmin == 'auto':
                cmin = np.min(cutout.data)
            else:
                cmin = float(vmin)

            if vmax == 'auto':
                cmax = np.max(cutout.data)
            else:
                cmax = float(vmax)
   
            # Use id to generate output filename
            cutout_filename = f"{src}_cutout.{fmt}"
            cutout_path = os.path.join(output_dir,cutout_filename)
            
            gc = plt.imshow(cutout.data,vmin=cmin,vmax=cmax,cmap=cmap)
            gc.axes.get_xaxis().set_visible(False)
            gc.axes.get_yaxis().set_visible(False)

            plt.savefig(cutout_path)


@click.command()
@click.option('--survey-dir',default='images',help='Directory for downloaded images')
@click.option('--output-dir',default='output',help='Directory for cutouts')
@click.option('--fmt',default='pdf',help='format for cutouts')
@click.option('--geometry',default='4x4',help='array layout')
@click.option('--vmin',default='auto',help='min value for colourmap')
@click.option('--vmax',default='auto',help='max value for colourmap')
@click.option('--cmap',default='afmhot',help='matplotlib colour table to use')
@click.argument('source_list', type=click.Path(exists=True), )
@click.argument('input_json', type=click.Path(exists=True), )
def make_bitmaps_array(source_list, input_json,survey_dir, output_dir, fmt, geometry, vmin, vmax, cmap):
    """Make directory of bitmaps cutout images"""
    # Read JSON file
    images, data = _process_JSON(input_json)

    # Get sources for each image
    image_srcs = _find_image_sources(images, data, survey_dir)

    # Read in source list
    srcs = pandas.read_csv(source_list,index_col="id")

    # Get geometry
    w = geometry.split('x')
    nx = int(w[0])
    ny = int(w[1])

    narr = nx*ny
    fontsize = 30/np.sqrt(narr)    

    # Use source list filename as prefix for output files
    fname = source_list.split('.')[0]

    # Made cutouts for each image
    i = 0
    page = 0
    for item in image_srcs:  
        image = item[0]
        print(f"Opening image file {item[0]}")

        # Open image fits file
        f1 = fits.open(image)
        data = f1[0].data
        header = f1[0].header
        w1 = wcs.WCS(header)
        pixel_scale = w1.pixel_scale_matrix[1, 1] * u.deg
        

        # Produce an image for each source
        for src in item[1]:
            cutout = _get_cutout(data, w1, srcs, src, pixel_scale)

            if vmin == 'auto':
                cmin = np.min(cutout.data)
            else:
                cmin = float(vmin)

            if vmax == 'auto':
                cmax = np.max(cutout.data)
            else:
                cmax = float(vmax)
               
            ax = plt.subplot(nx,ny,i+1)
            gc = plt.imshow(cutout.data,vmin=cmin,vmax=cmax,cmap=cmap)
            plt.text(0.1,0.85,src,fontsize=fontsize,color='white',transform=ax.transAxes)
            gc.axes.get_xaxis().set_visible(False)
            gc.axes.get_yaxis().set_visible(False)
            i += 1
            if i>=narr:
                i=0
                page += 1
                cutout_filename = f"{fname}_cutouts_page{page}.{fmt}"
                cutout_path = os.path.join(output_dir,cutout_filename)
                plt.tight_layout(pad=0.1, w_pad=0.1, h_pad=0.1)
                plt.savefig(cutout_path)
                print(cutout_path)
                plt.close()

    # Use id to generate output filename
#    fname = source_list.split('.')[0]
#    cutout_filename = f"{fname}_cutout.{fmt}"

    print(f"Page{page}")
    page += 1 
    cutout_filename = f"{fname}_cutouts_page{page}.{fmt}"
    cutout_path = os.path.join(output_dir,cutout_filename)
    plt.tight_layout()
    plt.savefig(cutout_path)
    print(cutout_path)

cli.add_command(make_fits)
cli.add_command(make_bitmaps)
cli.add_command(make_bitmaps_array)

if __name__ == '__main__':
    cli()
