from ast import arg
import click
import subprocess
import os, sys

#@click.group()
#def cli():
#    pass

@click.command()
@click.argument('input', type=click.Path(exists=True))
@click.option('--survey-dir',default='.',help='Directory for downloaded images')
@click.option('--output-dir',default='.',help='Director for cutouts')
@click.option('--format',default='fits',help='Output format png or fits')
@click.option('--vmin',default=None,help='min value for colourmap')
@click.option('--vmax',default=None,help='max value for colourmap')
@click.option('--cmap',default=None,help='matplotlib colour table to use')
@click.option('--geometry',default=None,help='Array tile mxn for pngs. None for individual pngs')
def cli(input, survey_dir, output_dir, format, vmin, vmax, cmap, geometry):
    """Process input source list to cutouts"""

    # Run process to find images where sources are located    
    res = subprocess.run(["check_LoTSS",input])

    # Check process ran okay
    if res.returncode!=0:
        print("Problem running check_LoTSS.py")
        sys.exit(-1)

    # Check survey_dir exists as a directory
    if not os.path.exists(survey_dir):
        print("Survey directory does not exist")
        sys.exit(-1)

    # Try downloading images, also check if already downloaded
    res = subprocess.run(["get_LoTSS_images",survey_dir])

    # Check process ran okay
    if res.returncode!=0:
        print("Problem running get_LoTSS_images.py")
        sys.exit(-1)

    # Now make arg list to make cutouts
    
    arg_list = ["sort_json"]

    if format=="fits":
        arg_list.append("make-fits")
        arg_list.append(f"{input}")
        arg_list.append(f"data.json")
    else:
        if geometry!=None:
            arg_list.append("make-bitmaps-array")
            arg_list.append(f"{input}")
            arg_list.append(f"data.json")

            if geometry!=None:
                arg_list.append("--geometry")
                arg_list.append(f"{geometry}")

        else:
            arg_list.append("make-bitmaps")
            arg_list.append(f"{input}")
            arg_list.append(f"data.json")

            
        arg_list.append("--fmt")
        arg_list.append(f"{format}")
        
        if vmin!=None:
            arg_list.append("--vmin")
            arg_list.append(f"{vmin}")

        if vmax!=None:
            arg_list.append("--vmax")
            arg_list.append(f"{vmax}")

        if cmap!=None:
            arg_list.append("--cmap")
            arg_list.append(f"{cmap}")

    arg_list.append("--survey-dir")
    arg_list.append(f"{survey_dir}")

    arg_list.append("--output-dir")
    arg_list.append(f"{output_dir}")
       
            

        

    print(arg_list)
    subprocess.run(arg_list)


#cli.add_command(make)

if __name__ == '__main__':
    cli()
