#!/bin/bash

singularity exec image_cutouts_singularity.sif cutouts --survey-dir ./Survey_Images --output-dir ./cutout_arrays  --format png --geometry 3x3 Sources.lis
