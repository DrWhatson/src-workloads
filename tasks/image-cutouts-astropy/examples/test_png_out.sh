#!/bin/bash
cutouts --survey-dir ./Survey_Images --output-dir ./cutout_images  --format png P000+23_src.lis
cutouts --survey-dir ./Survey_Images --output-dir ./cutout_images  --format png --vmin -1e-2 --vmax 2e-2 P013+26_src.lis
cutouts --survey-dir ./Survey_Images --output-dir ./cutout_images  --format png --vmin -5e-3 --vmax 1.5e-2 --cmap cividis P025+36_src.lis
cutouts --survey-dir ./Survey_Images --output-dir ./cutout_images  --format png --vmin -3e-3 --vmax 1e-2 --cmap magma P028+24_src.lis
