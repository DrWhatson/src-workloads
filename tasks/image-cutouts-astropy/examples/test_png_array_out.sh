#!/bin/bash
cutouts --survey-dir ./Survey_Images --output-dir ./cutout_arrays  --format png --geometry 3x3 Sources.lis 
cutouts --survey-dir ./Survey_Images --output-dir ./cutout_arrays  --format pdf --cmap inferno --vmin -5e-4 --vmax 3e-3 --geometry 5x4 Sources.lis 
