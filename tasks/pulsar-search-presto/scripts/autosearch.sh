#!/bin/bash

# Step1: RFI mitigation 
sh /scripts/step1.rfi.sh

# Step2: De-dispersion
sh /scripts/step2.dedispersion.sh

# Step3: FFT and red noise removal
sh /scripts/step3.fft.rednoise.sh

# Step4:  Accelsearch
sh /scripts/step4.accel.sh

# Step5: Candidate sifting
sh /scripts/step5.sifting.sh

# Step6: Folding
sh /scripts/step6.folding.sh