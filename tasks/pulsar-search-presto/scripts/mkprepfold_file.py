def process_line(line):
    parts = line.split('_')
    if len(parts) >= 2:
        dm = parts[-3].split()[0].split('M')[1]
        cand_number = parts[-1].split()[0].split(':')[1]
        prefix = '_'.join(parts[:-1])
        return prefix, cand_number, dm
    return None, None, None

def generate_command(prefix, cand_number, dm):
    return f"prepfold -ncpus 1 -n 128 -noxwin -noclip -nosearch -dm {dm} -accelcand {cand_number} -accelfile {prefix}_50.cand -o {prefix}_50 *fits"

file_name = "cands.txt"
target_number = '1221832280'
output_file = "prepfold.sh"

with open(output_file, 'w') as output:
    output.write("#!/bin/bash\n")

    with open(file_name, 'r') as file:
        for line in file:
            if target_number in line:
                prefix, cand_number, dm = process_line(line)
                if prefix and cand_number and dm:
                    command = generate_command(prefix, cand_number, dm)
                    output.write(command + "\n")
