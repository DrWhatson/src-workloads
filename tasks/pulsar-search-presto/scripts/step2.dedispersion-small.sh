#!/bin/bash
# get all cpus
n=`nproc`
# Step2: De-dispersion
prepsubband -ncpus $n -lodm 9.000000 -nsub 32 -dmstep 0.020000 -numdms 2 -downsamp 1 -zerodm -mask 1221832280_rfifind.mask -o 1221832280 splice_0001.fits