#!/bin/bash

# Step3: FFT and red noise removal
realfft *.dat

ls *DM*.fft | xargs -n 1 rednoise

rename -f 's/_red\.fft$/.fft/' *_red.fft
rename -f 's/_red\.inf$/.inf/' *_red.inf