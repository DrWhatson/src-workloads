# Pulsar Search task using PRESTO

This task will run pulsar-search on the SMART survey data, outputting a list of pulsar candidates and their properties.

The result will be in figures directory, and Pulsar J2145-0750 will be like this:
![](figures/1221832280_DM9.00_ACCEL_50_ACCEL_Cand_1.pfd.png)

## How to run the task

Clone this repository and add execute permissions to files in the repository

    chmod 755 -R *

We use a Makefile to organise different ways to run this task, depending on your need. The Makefile shows all the available options, but to run it fully automatically the following command will pull the latest container, download the data, and run the task:

    make run-full
    
You can also use the Makefile to run things separately step by step, such as pulling the image (`make pull-image`), downloading the data (`make get-data`), and running the task (`make run-task`). Since this task can take hours to complete, there is also a small version of this task that takes roughly 15 minutes `make run-task-small`.

The data download script `make get-data` gets the data directly from Zenodo, which also hosts some of the files needed when building the Dockerfile: https://zenodo.org/records/10989783

## Steps description

1. RFI mitigation

Search for radio frequency interference (RFI) with a certain time scale (2^N of time resolution speeds up the procedure) directly on the time series data (.fits or .fil), create a "mask" file to be screened later on. The number of bad intervals usually should be less than 20%.

This example:

    rfifind -noclip -ncpus 1 -time 13.1072 -o 1221832280 splice_0001.fits

2. De-dispersion

The second step is to correct the dispersion effect caused by the interstellar medium along the sight of the pulsar. We firstly need to determine an de-dispersion plan, which can be simply obtained using DDplan.py in PRESTO. The results are a table containing DM ranges, DM steps, and the downsample plan. In this work, we take total DM range from 1 to 50 pc cm^{-3}, and created a series of DM ranges with different DM steps for de-dispersion. Using "prepsubband" command as many times as the DM ranges, and the rfi mask from the first step above will be used. Below is an example:

    prepsubband -ncpus 1 -lodm 1.00 -nsubs 32 -dmstep 0.02 -numdms 100 -downsample 1 -zerodm -mask 1221832280_rfifind.mask -o 1221832280 splice_0001.fits


3. After each iteration of step 2, Fourier transform (FFT) the time series data of each de-dispersion result into the frequency domain.

    realfft *.dat

 Remove the rednoise of the power spectrum.

    ls *DM*.fft | xargs -n 1 rednoise

4. Find the periodic signals

using accelsearch in PRESTO, one can also search pulsar in binaries, adding zmax parameter for the binary orbiting acceleration. For single pulsar, zmax=0.

Example:

    ls *DM*.fft | xargs -n 1 accelsearch -ncpus 10 -inmem -numharm 16 -zmax 50

5. Candidate sifting

This step is to choose preferred candidates from accelsearch, while conditions can be modified. The ACCEL_sift.py program has to be put in the working folder. What most necessary to be modified is the sigma threshold. Here we set this value to be 2, and run:

    python 3 ACCEL_sift.py > cands.txt

6. Fold the candidates

With cands.txt one can fold the signals of all these candidates using prepfold in presto, however the informations should be read first. Using mkprepfold_file.py we creat another bash program with lists of candidates to be folded. As an example:

    prepfold -ncpus 1 -n 128 -noxwin -noclip -nosearch -dm 9.00 -accelcand 1 -accelfile 1221832280_DM9.00_ACCEL_50.cand -o 1221832280_DM9.00_ACCEL_50 *fits

The last step forms 4 files for each candidate as .pfd .pfd.bestprof .pfd.png and .pfd.ps. These figures and figure data will be put inside /figures folder. These final folded candidates can be further judged by eye or various AI classifiers. The later is important for survey search with thousands of tied-array beams and hundreds of thousand of candidates in one single observation.