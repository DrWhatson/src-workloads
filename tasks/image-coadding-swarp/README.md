# Image co-adding task using a containerised version of the software SWarp.

This task will co-add images that cover a similar area of sky, reducing the noise where images overlap. We use 4 images of the interacting galaxy pair ARP 240, seen in SDSS to demonstrate this.

**The SWarp website is here:** https://www.astromatic.net/software/swarp/

**The SWarp Github page is here:** https://github.com/astromatic/swarp

**The SWarp documentation (PDF download):** https://raw.githubusercontent.com/astromatic/swarp/legacy_doc/prevdoc/swarp.pdf


## How to run the task

Clone this repository and add execute permissions to files in the repository

    chmod 755 -R *

We use a Makefile to organise different ways to run this task, depending on your need. The Makefile shows all the available options, but to run it fully automatically the following command will pull the latest container, download the data, and run the task:

    make run-full
    
You can also use the Makefile to run things separately step by step, such as pulling the image (`make pull-image`), downloading the data (`make get-data`), and running the source-finding (`make run-task`).

Note that the parameter file `default.swarp` is written for this example and needs to be changed if using different images.

## How to use a Jupyter notebook

SWARP is run from the command line, but you can use a Jupyter notebook to run this using the terminal, and then plot the images using Python. The commands needed are included in the Makefile:

    make build-image-jupyter
    make run-local-jupyter

then a link will appear which you can copy into your browser to access.

You can run the `plot.ipynb` in the `/jupyter/scripts/` folder to create a plot.

### Outputs
When plotting the co-added image and the associated weight map, with the scale turned up like this you can see the variations in the RMS noise follow the boundaries in the combined images.

![Left: 4 co-added images combined in a weighted average. Right: weight map showing boundaries of the 4 images](./sdss-coadd.png)
Left: 4 co-added images combined in a weighted average. Right: weight map showing boundaries of the 4 images.

Zooming in on the galaxy pair you can see the noise is lower in the co-added image (left) and more diffuse details can be seen compared to a single image (right). The scales are matched in the image below.

![Left: 4 co-added images combined in a weighted average. Right: A single image on the same scale](./sdss-coadd-comparison.png)
Left: 4 co-added images combined in a weighted average. Right: A single image on the same scale.
