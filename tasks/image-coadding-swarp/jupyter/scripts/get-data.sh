# download 4 images covering M101 from the SDSS SAS

echo "downloading 4 fits files..."
wget http://dr17.sdss.org/sas/dr17/eboss/photoObj/frames/301/6174/2/frame-r-006174-2-0094.fits.bz2
wget http://dr17.sdss.org/sas/dr17/eboss/photoObj/frames/301/756/5/frame-r-000756-5-0595.fits.bz2
wget http://dr17.sdss.org/sas/dr17/eboss/photoObj/frames/301/1233/5/frame-r-001233-5-0038.fits.bz2
wget http://dr17.sdss.org/sas/dr17/eboss/photoObj/frames/301/1334/5/frame-r-001334-5-0056.fits.bz2

# decompress files
echo "decompressing bz2 files..."
bzip2 -d *.bz2

# running swarp
echo "co-adding images with swarp..."
swarp *.fits
