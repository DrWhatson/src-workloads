# EMUCAT ASKAP Pipeline

This is a cutback version of the EMUCat pipeline downloads an ASKAP observation, generates a mosaic and then performs a radio source extraction. 

## Requirements
 * SLURM HPC Cluster
 * Java > 11
 * Nextflow >= 21.04 
 * OpenMPI >= 4.1 
 * Singularity 

 Some of these might ce available as modules in the cluster envirnoment, which will have to be declared in the main.nf script to be loaded

Java can be installed in the user area by using SDKMAN and Nextflow as well

## Cluster declaration

The generic profile needs to be updated with details of your cluster in nextflow.config

```
generic {
    
        process {
            executor = 'local'
            //module = ['singularity'] // If singularity is provided by a module

            withName: run_selavy {     // Use batch job during source finding
                //module = ['mpich', 'singularity']	
                executor = 'slurm'
                clusterOptions = '--nodes=12 --ntasks-per-node=6'
                queue = 'cpu'
            }
        }
}
```

The number of processes needs to be matched to the number of areas the mosiac is divided into or sources are missed or duplicated. To check this make sure that (Selavy.nsubx x Selavy.nsuby) from templates/selavy.j2 equals the number of processors defined above

## Data 

The data are available from CASDA https://data.csiro.au/domain/casda for ASKAP Data Products for Project AS101 (ASKAP Pilot Survey for EMU): images and visibilities, if one has an account. One can easily make an OPAL account.

The data used was for SB10083 and SB10635:
```
image.i.SB10083.cont.taylor.0.restored.fits 
image.i.SB10083.cont.taylor.1.restored.fits 
weights.i.SB10083.cont.taylor.0.fits
weights.i.SB10083.cont.taylor.1.fits
image.i.SB10635.cont.taylor.0.restored.fits
image.i.SB10635.cont.taylor.1.restored.fits
weights.i.SB10635.cont.taylor.0.fits
weights.i.SB10635.cont.taylor.1.fits
```
One might be able to use the "save as links" to get the urls to use in the urls.json file. 
The download process script in main.nf has been modified to accept absolute paths to the data directory. This allows for staging the data beforehand to some temporary directory space.

The cloned emucat directory seems to be the best place to put the directory for the staged data and to set SCRATCH_ROOT to this emucat directory too. This helps keep the data within scope of singularity volume bindings 

## Running

```
module load nextflow
module load singularity

nextflow run main.nf  -profile generic --SCRATCH_ROOT=/path/to/emucat
```

* profile: nextflow.conf profile (default: generic)
* scratch_root: the base directory for all the data (default: /mnt/shared/test/)

## Results
The results appear in the data/selavy in a subdirectory EMU_xxxx_xxxx (depending on the job details, but EMU_2137_5300 for the case given)

This consists of a list of source components in a text file EMU_2137-5300_results.txt or in a xml VOtable EMU_2137-5300_votable.xml

There are more detailed files in txt and xml of the raw islands and components found to made the sources. Together with smoothed mean, noise and SNR maps used judge background for the sources. 


