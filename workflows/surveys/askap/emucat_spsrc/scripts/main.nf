nextflow.enable.dsl=2


process setup {
    
    input:
        val ser

    output:
        val ser, emit: ser_output

    script:
        """
        #!/bin/bash

        if [ -z "${ser}" ]
        then
            echo "ser is empty"
            exit -1
        fi

        mkdir -p ${params.SCRATCH_ROOT}
        mkdir -p ${params.OUTPUT_RAW}
        mkdir -p ${params.OUTPUT_LINMOS}
        mkdir -p ${params.OUTPUT_SELAVY}
        mkdir -p ${params.OUTPUT_LHR}
        mkdir -p ${params.OUTPUT_LOG_DIR}
        mkdir -p ${params.OUTPUT_EXTENDED_DOUBLES}
        """
}


process download {

    container = "aussrc/emucat_scripts"
    containerOptions = "--bind ${params.SCRATCH_ROOT}:${params.SCRATCH_ROOT}"

    errorStrategy 'retry'
    maxErrors 3

    input:
        val ser

    output:
        val "${params.OUTPUT_RAW}/manifest.json", emit: file_manifest

    script:
        """
        #!python3

        import urllib.request
        import shutil
        import json
        import os

        def is_url(s):
            # Check if the string is a URL.
            return s.startswith('http://') or s.startswith('https://')

        def is_file(s):
            # Check file path
            return os.path.exists(s)

        with open('$baseDir/urls.json', 'r') as infile:
            urls = json.loads(infile.read())['urls']

        images = []
        weights = []

        for f, u in urls:
            filename = f"${params.OUTPUT_RAW}/{f}"
            if 'image' in f:
                images.append(filename)
            elif 'weight' in f:
                weights.append(filename)

            if is_url(u):
                with urllib.request.urlopen(u) as response, open(filename, 'wb') as out_file:
                    shutil.copyfileobj(response, out_file)

            else:
	    
                in_file = f"{u}/{f}"
                if is_file(in_file):
                    shutil.copy(in_file, filename)

        files = {
            'images': images,
            'weights': weights}

        with open('${params.OUTPUT_RAW}/manifest.json', 'w') as outfile:
            outfile.write(json.dumps(files))
        """
}


process generate_linmos_conf {

    container = "aussrc/emucat_scripts"
    containerOptions = "--bind ${params.SCRATCH_ROOT}:${params.SCRATCH_ROOT}"

    input:
        path file_manifest
        val ser

    output:
        val "${params.OUTPUT_LINMOS}/linmos.conf", emit: linmos_conf
        val "${params.OUTPUT_LINMOS}/linmos.log_cfg", emit: linmos_log_conf

    script:
        """
        #!python3

        import json
        from jinja2 import Environment, FileSystemLoader
        from pathlib import Path

        with open('${file_manifest.toRealPath()}') as o:
           data = json.loads(o.read())

        images = [Path(image).with_suffix('') for image in data['images'] if '.0.' in image]
        weights = [Path(weight).with_suffix('') for weight in data['weights'] if '.0.' in weight]
        image_out = Path('${params.OUTPUT_LINMOS}/${ser}.image.taylor.0')
        weight_out = Path('${params.OUTPUT_LINMOS}/${ser}.weights.taylor.0')
        log = Path('${params.OUTPUT_LOG_DIR}/${ser}_linmos.log')

        j2_env = Environment(loader=FileSystemLoader('$baseDir/templates'), trim_blocks=True)
        result = j2_env.get_template('linmos.j2').render(images=images, weights=weights, \
        image_out=image_out, weight_out=weight_out)

        with open('${params.OUTPUT_LINMOS}/linmos.conf', 'w') as f:
            print(result, file=f)

        result = j2_env.get_template('yandasoft_log.j2').render(log=log)

        with open('${params.OUTPUT_LINMOS}/linmos.log_cfg', 'w') as f:
            print(result, file=f)
        """
}


process run_linmos {

    container = "csirocass/askapsoft:1.9.1-casacore3.5.0-mpich"
    containerOptions = "--bind ${params.SCRATCH_ROOT}:${params.SCRATCH_ROOT}"

    input:
        path linmos_conf
        path linmos_log_conf
        val ser

    output:
        val "${params.OUTPUT_LINMOS}/${ser}.image.taylor.0.fits", emit: image_out
        val "${params.OUTPUT_LINMOS}/${ser}.weights.taylor.0.fits", emit: weight_out

    script:
        """
        #!/bin/bash

        if [ ! -f "${params.OUTPUT_LINMOS}/${ser}.image.taylor.0.fits" ]; then
            linmos -c ${linmos_conf} -l ${linmos_log_conf}
        fi
        """
}


process generate_selavy_conf {

    container = "aussrc/emucat_scripts"
    containerOptions = "--bind ${params.SCRATCH_ROOT}:${params.SCRATCH_ROOT}"

    input:
        path image_input
        path weight_input
        val ser

    output:
        val "${params.OUTPUT_SELAVY}/selavy.conf", emit: selavy_conf
        val "${params.OUTPUT_SELAVY}/selavy.log_cfg", emit: selavy_log_conf

    script:
        """
        #!python3
        from jinja2 import Environment, FileSystemLoader
        from pathlib import Path
        import os
        from astropy.io import fits

        # Correct header
        #fits.setval('${image_input.toRealPath()}', 'BUNIT', value='Jy/beam ')

        ser = '${ser}'
        output_path = Path('${params.OUTPUT_SELAVY}')
        image = Path('${image_input.toRealPath()}')
        weight = Path('${weight_input.toRealPath()}')
        log = Path('${params.OUTPUT_LOG_DIR}/${ser}_selavy.log')
        results = Path('${params.OUTPUT_SELAVY}/${ser}_results.txt')
        votable = Path('${params.OUTPUT_SELAVY}/${ser}_votable.xml')
        annotations = Path('${params.OUTPUT_SELAVY}/${ser}_annotations.ann')

        j2_env = Environment(loader=FileSystemLoader('$baseDir/templates'), trim_blocks=True)
        result = j2_env.get_template('selavy.j2').render(ser=ser, output_path=output_path, image=image, weight=weight, \
                 results=results, votable=votable, annotations=annotations)

        with open('${params.OUTPUT_SELAVY}/selavy.conf', 'w') as f:
            print(result, file=f)

        result = j2_env.get_template('yandasoft_log.j2').render(log=log)

        with open('${params.OUTPUT_SELAVY}/selavy.log_cfg', 'w') as f:
            print(result, file=f)
        """
}


process run_selavy {

    input:
        path selavy_conf
        path selavy_log_conf
        val ser

    output:
        val "${params.OUTPUT_SELAVY}/${ser}_results.components.xml", emit: cat_out
        val "${params.OUTPUT_SELAVY}/${ser}_results.islands.xml", emit: island_out

    script:
        """
        #!/bin/bash

        if [ ! -f "${params.OUTPUT_SELAVY}/${ser}_results.components.xml" ]; then
            srun --export=ALL --mpi=pmi2 -n 36 \
                   singularity exec \
                   --bind ${params.SCRATCH_ROOT}:${params.SCRATCH_ROOT} \
                   ${params.IMAGES}/csirocass-askapsoft-1.9.1-casacore3.5.0-mpich.img \
                   selavy -c ${selavy_conf} -l ${selavy_log_conf}
        fi
        """
}


workflow emucat_ser {
    take:
        ser

    main:
        setup(ser)

        // Download Image data
        download(setup.out.ser_output)

        // Generate Super-Mosaic
        generate_linmos_conf(download.out.file_manifest, ser)
        run_linmos(generate_linmos_conf.out.linmos_conf, generate_linmos_conf.out.linmos_log_conf, ser)
        
        // Source extraction
        generate_selavy_conf(run_linmos.out.image_out, run_linmos.out.weight_out, ser)
        run_selavy(generate_selavy_conf.out.selavy_conf, generate_selavy_conf.out.selavy_log_conf, ser)
}


workflow {
    main:
        emucat_ser(params.ser)
}
