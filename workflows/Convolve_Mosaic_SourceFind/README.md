# A test Nextflow pipeline to combine Convolve, Mosaic and SourceFind Tasks

The Nextflow language and install instructions: https://www.nextflow.io/docs/latest/getstarted.html


This workload is a test of using the Nextflow workflow language to string together sandalone tasks into a pipeline. As such it takes three tasks from this repository; image-convolution, mosaicking-swarp and source-finding-pybdsf, to smooth inputs maps prior to coadding them into a biger maps which is then the pyBDSF source finder is run on to make a catalogue.
In order to get the tasks to work together some extra code is needed to run between tasks to make the data compatible, which is taken care of in the Nextfllow main.nf script

## Parameters to set
In order to run the user will need to set the parameters in the main.nf script for

`params.SRC_TASKS_DIR` to give the full pathname to the tasks directory of the cloned src-workflows repo

and 

`params.SCRATCH_ROOT` for directory to run the script (usually a copy of the Convolve_Mosaic_SourceFind directory)

Another parameter is `params.sigma` to set the gaussian smoothing in pixels

The first step is the downloading of the data listed in `lofar-files.csv` [They don't need to be LOFAR images, but that's the data the first task expects and also the eample data used]
The format for each entry is one line with two fields separated by a comma. First field being the map url and thge second the name for the files.

```csv
https://lofar-surveys.org/public/DR2/mosaics/P007+39/low-mosaic-blanked.fits, P007+39.fits
```

## How to run pipeline

Assuming nextflow is on your PATH
```bash
    $ nextflow run scripts/main.nf
```
## The given example

The example given takes three overlapping low-resolution LOFAR images, applies a 5-pixel gaussian smoothing, coadds the images into one and run pyBDSF on it.

This should take between 5 and 10 minutes finding 4442 sources

```
$ nextflow run main.nf 
N E X T F L O W  ~  version 23.10.1
Launching `main.nf` [furious_boyd] DSL2 - revision: 65374d932a
executor >  local (4)
[53/b04323] process > download    [100%] 1 of 1 ✔
[01/1c68df] process > convolution [100%] 1 of 1 ✔
[5d/1179ee] process > mosaicking  [100%] 1 of 1 ✔
[b6/c20052] process > source_find [100%] 1 of 1 ✔
Completed at: 19-May-2024 20:12:10
Duration    : 5m 32s
CPU hours   : 0.1
Succeeded   : 4
```

A ```work``` sub-directory will be created with the task code being copied to further sub-directories and executed.

A ```results``` sub-directory is also created which stores the final output catalogue and intermediate products

```
results/
├── catalogs
│   └── 19May2024_20.07.04
│       ├── background
│       │   ├── P21-low-mosaic.pybdsf.mean_I.fits
│       │   ├── P21-low-mosaic.pybdsf.norm_I.fits
│       │   └── P21-low-mosaic.pybdsf.rmsd_I.fits
│       ├── catalogues
│       │   ├── P21-low-mosaic.pybdsf.gaul
│       │   ├── P21-low-mosaic.pybdsf.gaul.ds9.reg
│       │   ├── P21-low-mosaic.pybdsf.gaul.FITS
│       │   ├── P21-low-mosaic.pybdsf.kvis.ann
│       │   ├── P21-low-mosaic.pybdsf.lsm
│       │   ├── P21-low-mosaic.pybdsf.sky_in
│       │   ├── P21-low-mosaic.pybdsf.srl
│       │   ├── P21-low-mosaic.pybdsf.srl.ds9.reg
│       │   ├── P21-low-mosaic.pybdsf.srl.FITS
│       │   └── P21-low-mosaic.pybdsf.star
│       ├── misc
│       │   ├── island_file
│       │   ├── parameters.sav
│       │   └── parameters_used
│       ├── model
│       │   └── P21-low-mosaic.pybdsf.model.fits
│       └── residual
│           └── P21-low-mosaic.pybdsf.resid_gaus.fits
├── convolved_maps
│   ├── P007+39.fits
│   ├── P009+36.fits
│   └── P010+39.fits
├── data
│   ├── P007+39.fits
│   ├── P009+36.fits
│   └── P010+39.fits
└── mosaic_map
    ├── coadd.fits
    └── coadd.weight.fits

11 directories, 26 files
```


