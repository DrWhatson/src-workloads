nextflow.enable.dsl=2

// Define where the main directory is
params.SCRATCH_ROOT     = "/home/ubuntu/src-workloads/workflows/Convolve_Mosaic_SourceFind/scripts" 

// Where a copy of src-workflow repo is
params.SRC_TASKS_DIR    = "/home/ubuntu/src-workloads/tasks" 

// Where to put results
params.RESULTS_DIR     = "{params.SCRATCH_ROOT}/../results"

// SRC_Workflow tasks
params.MOSAIC_DIR       = "${params.SRC_TASKS_DIR}/mosaicking-swarp"
params.PYBDSF_DIR       = "${params.SRC_TASKS_DIR}/source-finding-pybdsf"
params.CONV_DIR         = "${params.SRC_TASKS_DIR}/image-convolution"

// The convolution workflow
//params.CONV_DIR = "/home/bob/workflow_convolution_repo"     

// List of files to download
params.fits_list        = "${params.SCRATCH_ROOT}/lofar-files.csv"

// Files to run convolution pipeline
params.conv_pipeline    = "${params.SCRATCH_ROOT}/conv_pipeline.sh"
params.conv_run         = "${params.SCRATCH_ROOT}/conv_run.sh"
params.fits_beam_update = "${params.SCRATCH_ROOT}/fits_beam_update.py"
// Smoothing parameter (in pixels)
params.sigma = "5.0"

// Files and paramters for mosaic
params.mosaic_run       = "${params.SCRATCH_ROOT}/mosaic_run.sh"
params.mosaic_defaults  = "${params.SCRATCH_ROOT}/default.swarp"

// Files to run source finding
params.fits_mod = "${params.SCRATCH_ROOT}/modify_fits.py"
params.pybdsf_run = "${params.SCRATCH_ROOT}/pybdsf_run.sh"


process download {

    publishDir "${params.RESULTS_DIR}" , mode: 'copy'

    input:
        path data_list
	
    output:
        path data, emit: fits_dir  

    script:
        """
        #!/bin/bash

	    cp $data_list .  # Copy over list of fits maps

        mkdir data
        cp ${params.SCRATCH_ROOT}/download_LoRes_Lofar.py .
        python3 download_LoRes_Lofar.py
	
        """
}
		    

process convolution {

    publishDir "${params.RESULTS_DIR}" , mode: 'copy'

    input: 
        path data
	    path conv_pipeline
	    path conv_run
	    val sigma
	
    output:
        path convolved_maps, emit: conv_data 
        
    script:
        """
	    #!/bin/bash
        
	    mkdir convolved_maps	
	    cp -r ${params.CONV_DIR}/. .	
	    cp ${params.fits_beam_update} .
	    
        rm run.sh
        cp $conv_run run.sh

	    cp $conv_pipeline scripts/image_gauss_conv.sh
        chmod 777 scripts/image_gauss_conv.sh
	
	    ./run.sh ${sigma}

        python3 fits_beam_update.py convolved_maps ${sigma}         
	""" 	
}


process mosaicking {

    publishDir "${params.RESULTS_DIR}" , mode: 'copy'

    input:
        path conv_data
        path mosaic_run
	
    output:
        path mosaic_map, emit: coadd_path

    script:
        """
        #!/bin/bash

	    cp -r ${params.MOSAIC_DIR}/. .
        chmod 777 scripts/lofar/make-lofar-mosaic.sh

        ln -s $conv_data data

        cp ${params.mosaic_defaults} data/.

        make pull-image
		./mosaic_run.sh

        mkdir mosaic_map
        mv data/coadd* mosaic_map/.

        """
}

process source_find {

    publishDir "${params.RESULTS_DIR}" , mode: 'copy'

    input:
        path coadd_data
        path pybdsf_run

    output:
        path catalogs


    script:
        """
        #!/bin/bash
        cp -r ${params.PYBDSF_DIR}/. .
        cp ${params.SCRATCH_ROOT}/modify_fits.py .

        mkdir data
        
        python3 modify_fits.py mosaic_map/coadd.fits data/P21-low-mosaic.fits

        chmod 777 scripts/LOTSS-P21-sourcefinding.sh

        make pull-image

        ./pybdsf_run.sh

        mkdir catalogs
        cp -r data/P21-low-mosaic_pybdsf/. catalogs 

        """
}


workflow {
    
    main:
        download(params.fits_list)
	    //download.output.fits_dir.view()
	
	    convolution(download.output.fits_dir,
params.conv_pipeline, params.conv_run, params.sigma)
	    //convolution.output.conv_data.view()
	
        mosaicking(convolution.output.conv_data, params.mosaic_run) 

        source_find(mosaicking.output.coadd_path, params.pybdsf_run)
}
