#!/bin/bash

docker run --rm --name mosaicking-swarp -v $PWD/scripts:/scripts/ -v $PWD/data:/data/ registry.gitlab.com/ska-telescope/src/src-workloads/mosaicking-swarp:latest

