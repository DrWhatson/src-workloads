import requests
import os

def download_file(url, save_path, filename):
    try:
        # Send a HTTP request to the URL
        response = requests.get(url, stream=True)
        response.raise_for_status()  # Check if the request was successful
        
        # Create the full path to save the file
        full_path = os.path.join(save_path, filename)
        
        # Write the content to a file
        with open(full_path, 'wb') as file:
            for chunk in response.iter_content(chunk_size=8192):
                file.write(chunk)
        
        print(f"Downloaded: {url} to {full_path}")
    except requests.exceptions.RequestException as e:
        print(f"Failed to download {url}: {e}")

def download_files_from_list(file_path, save_path):
    # Ensure the save directory exists
    os.makedirs(save_path, exist_ok=True)
    
    # Read the list of URLs and filenames from the specified file
    with open(file_path, 'r') as file:
        lines = file.readlines()
    
    # Download each file from the list
    for line in lines:
        line = line.strip()  # Remove any leading/trailing whitespace
        if line:
            url, filename = line.split(', ')
            download_file(url, save_path, filename)

if __name__ == "__main__":
    # File containing the list of URLs and filenames
    urls_file = 'lofar-files.csv'
    
    # Directory to save the downloaded files
    save_directory = 'data'
    
    download_files_from_list(urls_file, save_directory)
