#!/bin/bash
# this script fixes permissions inside the docker container,

# fix permissions
chmod 755 * -R

# run the code
python3 /scripts/image_gauss_conv_multiprocessing.py --ncpus 8 --sigma $@ --dirIN /data --dirOUT /conv --pattern '*.fits' --prefix ''