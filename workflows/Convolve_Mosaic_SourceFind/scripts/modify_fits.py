from astropy.io import fits
import numpy as np
import sys

in_fits = sys.argv[1]
out_fits = sys.argv[2]

# Load the image
hdu = fits.open(in_fits)

hdu[0].header['CDELT1'] = hdu[0].header['CD1_1']
hdu[0].header['CDELT2'] = hdu[0].header['CD2_2']

data = hdu[0].data
dataflat = data.flat
ip = np.where(dataflat==0.0)[0]
dataflat[ip] = np.nan
hdu[0].data = data


hdu.writeto(out_fits)

