from astropy.io import fits
import numpy as np
import sys, os

fits_dir = sys.argv[1]
sig_pix = float(sys.argv[2])   # Sigma in pixels

def update_header(fits_in, sig_pix):

    hdu = fits.open(fits_in)

    cdelt2 = hdu[0].header['CDELT2'] # Get pixel scale
    sigma = sig_pix * 3600 * cdelt2

    has_bmaj = 'BMAJ' in hdu[0].header

    if has_bmaj:
        bmaj = hdu[0].header['BMAJ']
        bmin = hdu[0].header['BMIN']
        bpa  = hdu[0].header['BPA']

        new_bmaj = np.sqrt(bmaj**2 + (sigma/3600)**2)
        new_bmin = np.sqrt(bmin**2 + (sigma/3600)**2)
    else:
        new_bmaj = sigma/3600
        new_bmin = sigma/3600
        bpa = 90

    hdu[0].header['BMAJ'] = new_bmaj
    hdu[0].header['BMIN'] = new_bmin
    hdu[0].header['BPA'] = bpa

    hdu.writeto(fits_in, overwrite=True)

try:
    files = os.listdir(fits_dir)

    for f in files:
        file = f"{fits_dir}/{f}"
        update_header(file,sig_pix)

except Exception as e:
    print(f"Error: {e}")
    sys.exit(1)
