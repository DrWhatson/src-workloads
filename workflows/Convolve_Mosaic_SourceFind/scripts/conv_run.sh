#!/bin/bash

docker build -t img_gauss_conv .
docker run --rm --name img_gauss_conv -v $PWD/scripts:/scripts/ -v $PWD/data:/data/ -v $PWD/convolved_maps:/conv/  img_gauss_conv $1