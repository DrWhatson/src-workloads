#!/bin/bash

docker run --rm --name source-finding-pybdsf -v $PWD/scripts:/scripts/ -v $PWD/data:/data/ registry.gitlab.com/ska-telescope/src/src-workloads/source-finding-pybdsf:latest
