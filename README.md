- [SRC Scientific Workflows](#src-scientific-use-cases)
- [Directory structure](#directory-structure)
- [Adding a new applications or workflow](#adding-a-new-applications-or-workflow)

# SRC Scientific Workloads

## Introduction 

The goal of this repository is to collect runnable scientific **workloads** that will be expected to run at SKA Regional Centers (SRC). We classify workloads into **tasks** and **workflows**. A task is a small piece of code that achieves a step towards a science result, this could be source-finding, mosaicking, image cutting, etc. A workflow is a collection of these tasks that achieve a more detailed scientific result, and we are investigating using a workflow language to achieve this.

Each of the tasks and workflows should contain all the necessary information to be reproduced. This means any software required should be in a container with the Docker file described in the repository and integrated into our CI pipeline. A Makefile should describe commands to pull the image from the container repository, download the data and run the task.

### Workload mandatory requirements

Each workload should meet the following minimal requirements to ensure that they can be run consistently and reproducibly.

  - A **`README.md`** file containing at least:
	- A general description of the workload and purpose.
  	- Details of the software structure in your task, such that users understand how to tweak parameters and execute runs using alternative datasets. Versions and dependencies should be stated.
	- A description of the expected outputs:
	  - Approximate runtime or performance results.
	  - Do not include output data products, but do include screenshots or values that show the expected result for a successful run.
	- Hardware dependencies. This involves specifying whether the code supports accelerators (such as GPUs), and whether these accelerators are required or optional (e.g. the Makefile in the radio-galaxy-classifier-cnn has CPU and GPU otions). State any other hardware dependencies (e.g. if the code is only compatible with a specific network fabric, detail this).
  - Required files: **./Dockerfile**, **./Makefile**, **/scripts**, **/data**, and a script set as the entrypoint in the Dockerfile (e.g. **/scripts/pipeline.sh**) to run the task.
    - The Data directory is where any data products will be downloaded to prior to running the task. No data should be stored in here on the repository. /data will be the working directory in the container, so any output products also end up in here. 
    - /scripts is where all scripts should be stored.
    - The dockerfile should set `workdir=/data`. The entrypoint should be set in the Dockerfile, `entrypoint=["/scripts/pipeline.sh"]` so that we can use `docker run` to execute the tasks consistently. `/scripts/pipeline.sh` is just an example, and could be a Python script or any other script that runs the task. For example, you could have calls to `sourcefinding-large.py` and `sourcefinding-small.sh` inside of `pipeline.sh` to give examples running on large and small data sets. These can be specified by setting the --entrypoint in the docker run command in the Makefile for alternative runs. Other directories could be mounted if you need to store intemediate data products or outputs in a cleaner way.
  	- A Makefile sets out all the commands you could run in a clean way. Check other tasks for examples you can copy. It means you can do `make pull-image`, `make get-data`, and `make run-task` or `make run-task-GPU` to separate out the different components. 
    - When the docker container is run `docker run -it --rm ...` it will execute the `./pipeline.sh` script (because of the entrypoint in the dockerfile), and shut down after completing this script. This entrypoint could be overriden with a specific Makefile entry `make run-task-version2` which references a `docker run --entrypoint...` command.
  - You can always do a `docker exec` command to use the container interactively for testing/debugging, but note that you will need to shut it down by hand afterwards.
  - For the CI pipeline in this repo to automatically upload your container to the registry, your container should be named and tagged the same as the task name and **it must be all lower-case**.
  - Please edit the .gitlab-ci.yml in the repo to add your new task name to the CI_JOB_NAME list at the end of the file, and ensure it builds correctly (it will only automatically build when the Dockerfile changes so you may need to re-save it to see the CI job start). Your container will then be automatically added to the container repository. 
  - Please reach out to a maintainer if you need assistance with any of these steps. Adding a new task means you are happy to collaborate on maintaining it and discussing updates via merge requests. 

**Please see examples in tasks such as source-finding-pybdsf, mosaicking-swarp, image-coadding-swarp.**

### Workload optional data

  In addition, we recommend adding the following optional information to each workload. These are especially relevant for tasks:

  - *In the README.md*: How to use the task with alternative data, e.g. editing relevant parameter files and highlighting any potential problems with unique data sets.
  - *Linked in the README.md*: A recorded demo of how to run the task.
  - *In a `contrib/` directory*: Include any relevant runtime parameters such as CPU affinity settings, recommended memory/core ratio, and performance scalability properties of the workload (strong scaling and/or weak scaling). You're welcome to include any data that helps understand the workload behaviour and aid in running it efficiently, such as batch submission scripts and/or graphs.
  - *In a `contrib/` directory*: Power and energy efficiency data (aggregated or time series).
  - *In a `contrib/` directory*: Profiling data, and accompanying performance analysis results or conclusions.
  - Automatic running of the workload in a remote site via CI pipelines.
  - A build script in `scripts/build.sh`, meant for end users who want to build the container images themselves. Note that the CI system will not use this script, so the container needs to build as described above.

## Adding a new Task or Workflow

If you would like to add a new task or workflow, the first thing you need to do is plan this feature well before a PI planning event. The feature should be refined with the input from Alex Clarke and others in program team, to assess the business value and priority, and to ensure it fits well within the scope of this repository. Once a feature is planned, one of the acceptance criteria should be that it is added via a merge request to this repo, and a video demo is made of the task.

Adding a new Task or Workflow should replicate the example structure detailed above. We want all Tasks to be directly hosted in this repo, but exceptions can be made where it is more appropriate to add them as sub-modules that mirror another repo. Large workflows may already be hosted in other Git repositories, and so adding them as sub-modules may be preferred. Note however that we use CI/CD to build containers automatically and batch test tasks, which requires the task to be directly hosted in this repo. If you are on the SKAO Gitlab account you will be a "developer" who can create feature branches and do merge requests. A "maintainer" will then approve the merge request. 

To add a new Task, you should do the following from the command line:

Create a new feature branch and then submit a merge request following these instructions: https://docs.gitlab.com/ee/gitlab-basics/feature_branch_workflow.html

At stage 4 of those instructions, if you need to add it as a sub-module do the following (however we are discouraging the use of sub-modules, since it will not work with our CI pipeline and we would like all code hosted directly in this repo):

Move to the task directory:

`cd src-workloads/tasks`

Add the Git repo for your task as a sub module, where the path is the http url, and give it an appropriate folder name:

`git submodule add <path-to-your-task-repo> <folder-name-for-task>`

Then continue with steps 5/6/7/8/9 in the above instructions to push the changes to the branch you created and then submit a merge request to the main branch. A maintainer will then approve your merge request and the branch will be merged into the main. 

## STARS benchmarking scores

In `/bench` there is a file called suite.yaml which specified all the tasks being used as benchmarks. Parameters in this file should not be changed by users, but any user can benchmark their own system by running `python3 stars.py suite.yaml`. All tasks will then be run 5 times (plus one warm up time), and runtimes will be reported and compared to the reference system. This is output in a file called STARS_logfile.txt. Currently this data is being collected on [this confluence page](https://confluence.skatelescope.org/pages/resumedraft.action?draftId=252393028&draftShareId=ab337909-3beb-4e48-b175-8fb0e516f542&). This benchmarking command will take many hours to complete, but also if you do run individual tasks you can report how you use them with your hardware in the second table on the linked confluence page. Eventually benchmarking scores will be collected in a dashboard automtically (via stars.py) without the need to report them on a confluence page, and difference systems can be compared there.

## Directory structure

In order to be able to structure and scale all of this information, this repository is organised in the following hierarchical structure. The mosaicking-swarp Task is expanded as an example, with items in bold being mandatory:

<pre>
├── README.md
├── Tasks  # Tasks are independent steps towards a scientific result.
│   ├── Image-coadding-swarp
│   ├── Source-finding-pybdsf
│   ├── Image-cutouts-astropy
│   └── Mosaicking-swarp
│       ├── <b>Dockerfile</b>
│       ├── <b>Makefile</b>
│       ├── <b>README.md</b>
│       ├── <b>data</b>
│       └── <b>scripts</b>
│          ├── lofar
│          │   ├── get-data-LOTSS.sh
│          │   ├── make-lofar-mosaic.sh
│          │   └── lofar-files.csv
│          └── sdss
│              ├── get-data-SDSS.sh
│              ├── make-SDSS-mosaic.sh
│              └── J011700.00+280000.0.small.sh
│   
├── Workflows # Workflows are a collection of independent tasks, with scripts that wrap them together so a user can obtain a complex science result.
│   ├── HI-FRIENDS
│   ├── eMerlin
│
└── System-tests
    ├── Distributed-analysis-dask
</pre>

We expect to organise tasks further into categories. It is unclear how broad these categories will be. We plan to decide this once we gather a significant amount of tasks, and it becomes more apparent how to scale these into categories.

Workflows consists of one or more tasks, and each task runs one application with certain inputs. Each task can be run multiple times, resulting in individual runs where certain parameters (input stays the same) may change, such as number of nodes/processes, the SRC node, or on the computation device. Workflows may be maintained as separate git repositories. Ideally, workflows reference any tasks used from this repository, but could include more unique aspects inside the workflow repository. For example, if a workflow does mosaicking, source-finding and source classification, each of those steps would be separate tasks, and they would be brought together using a workflow description language that references the tasks and provides all the information required to stitch them together into a complete workflow run. A workflow may include tasks that are not yet integrated into this repository, and so this should be stated clearly in the workflow with a note on if it makes sense to include this as a task in the future. 

Note: The exact structure for workflows is yet to be determined once we have a fully working example.

## Limitations and future challenges

We're aware of the following limitations and challenges that will have to be overcome as we attempt to run these workloads accross the SRCNet:

- Most HPC centers do not support Docker runtime at all (Docker **images** are usually fully supported). Often, only other runtime engines (such as Singularity/Apptainer or Sarus) are supported for security and operational reasons.  Therefore, some generic way of launching workloads will have to be found that goes beyond the current approach. We can't just have a `docker run` and expect things to work on remote sites.
	- One simple option would be using a shim wrapper.
	- Another solution would be leveraging future Compute Services APIs to launch a workload on remote SRCNet sites.
- Containers are built with portability in mind, which can have a severe negative impact on performance. This is not a specific issue to this project, but more globally for the SRCNet and the scientific/HPC community. For instance, solutions will have to be found to avoid having containers use the most common denominator that will run anywhere, which would by definition not be able to exploit the full performance offered by sites. Conversely, if a container is built with certain performance optimizations, this might exclude certain sites from being able to run this optimized workload. Therefore, a mechanism will be needed to select compatible sites for a given workload. Examples of optimizations that attempt to achieve high performance include (not an exhaustive list):
	- Using an appropriate MPI runtime, especially the networking components, can have a huge impact on network latency and throughput for MPI applications.
	- CPU codes that are compiled to use certain accelerated instructions (such as the AVX family, but there are others), means that a certain CPU "flavour" is required.
	- GPU codes built with a specific compiler version might have a minimal dependency on system software. (e.g. an nvidia compiler usually requires a minimal CUDA version).
