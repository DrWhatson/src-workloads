# SRC Network Representative Performance Score

**S**RC ne**T**work represent**A**tive pe**R**formance **Score** (**STARScore**, or **STARS** for short)

This benchmark suite runs a series of representative SRCNet benchmarks, as defined in a YAML configuration file, and outputs the resulting performance STARScore.

It computes the score by measuring the average runtime of each benchmark. The runtime of each benchmark is compared to that of a reference system in order to obtain intermediate relative scores. Finally, weighted geometric mean of these scores is computed to produce a final score: the STARScore/STARS unit.

## Disclaimer

**The name STARS and the benchmark suite itself is purely a prototype/experiment/suggestion at this point, and has not been agreed on at any level by the SRCNet members**.

## Why?

Why a performance metric like STARS instead of something simpler or easier to compute, such as FLOPS or node-hours?

Node-hours is a simple and effective metric for clusters of homogeneous computing resources, since these can be compared in an apples-to-apples fashion.
However, the compute resources of the SRCNet will be anything but homogeneous.
Therefore, the node-hours metric becomes very impractical: 1 node hour of site A could mean something very different to 1 node hour of site B. In fact, even within site A there might be heterogeneity that renders the node-hour metric ineffective.

FLOPS is often used as well. However, FLOPS is a metric which is often not representative of real world performance.
First, peak theoretical performance is rarely achieved.
Second, systems with similar FLOPS might deliver vastly different performance levels with real-world applications.
Finally, the FLOPS metric also fails to account for the differences in workload precision requirements, which can significantly impact performance.

Computing the **S**RC ne**T**work represent**A**tive pe**R**formance **S**core has several advantages.
STARS does not suffer from the limitations of other metrics as outlined above. The STARScore is leveraging a set of representative SRCNet workloads, and is giving a measure of how much faster a system is in relation to a reference system.

As a result, it can be a useful metric for the following scenarios:
- *Pledging* and *accounting*. Using a representative performance metric such as STARScore is useful to account for the amount of *useful computation* (for SRCNet purposes) that a site is able to offer, as well as keeping track of how many resources were consumed for accounting purposes. STARS-hours can easily become a currency for compute at the global SRCNet level.
- *Procurement*. Institutions participating in the SRCNet that need to procure computing systems are able to leverage STARScore as a metric for comparing the computational capability of systems in the context of the SRCNet. For instance, it becomes possible to use STARS/EUR or STARS/USD to rate one offer over another, in a way that's meaningful for SRCNet purposes.
- *Energy and Power Efficiency*. Similarly, it becomes possible to determine the amount of *useful computation* per energy (STARS/Wh) or power efficiency of a system (STARS/W) in the context of the SRCNet.

## Requirements

- Python 3.6 or higher
- Install library dependencies with: `pip install -r requirements.txt`.

## Usage

To run the benchmark suite, run the following command in the terminal:

```bash
python stars.py [-d] path/to/suite.yaml
```

`-d` or `--debug`: Increase log level to debug (optional).

`--reference-system` or `--runtimes-only`: Instead of computing a score, it prints the average runtimes only.
The output will be in a yaml format that is usable as a reference system.

`path/to/suite.yaml`: The path to the YAML file containing the benchmark suite definition (mandatory).

## YAML Configuration File Format

The YAML file contains global settings and a list of benchmarks to run. Below is a detailed explanation of the YAML configuration format with sample values:

```yaml
globals:
  warmup: 1  # Number of times to run the benchmark as a warm-up. These runs will be discarded. (default: 0).
  repeat: 5  # Number of times to repeat each benchmark (default: 5).
  cv_threshold: 10  # Coefficient of Variation (CV) threshold as a percentage (default: 5%).
                   # A benchmark run exceeding this CV threshold will be rejected for high variability.

benchmarks:
  - name: "radio-galaxy-classifier-cnn"  # Unique name of the benchmark.
    prescript: "make pull-image; make get-data" # anything you need to do which isn't part of the benchmark.
    runscript: "make run-task-cpu"
    weight: 1 # Default weight is 1. Weight from each benchmark will be used to compute the final score.

reference_system:
    radio-galaxy-classifier-cnn:
          runtime: 78.55
```

Each benchmark must have a unique name and a script to run. The fields that have a default are optional.

### Reference System

The `reference_system` section of the YAML file contains the expected runtimes for each benchmark on a reference system. It can be generated by running stars with the `--reference-system` option. The benchmark name in the `reference_system` section must match the corresponding benchmark name in the `benchmarks` section.

## Output

The script outputs individual/intermediate benchmark scores, the final STARScore, and logs any errors or warnings during execution. If any benchmark has a coefficient of variation exceeding the threshold, the run is marked as invalid, and a warning is printed.

## Contributing

Please feel free to submit pull requests with any improvements or file an issue if you encounter any bugs.
