import argparse
import yaml
import subprocess
import numpy as np
import os
import sys
import logging
logging.basicConfig(filename='STARS_logfile.txt', level=logging.INFO) # logging file doesn't seem to be created unless done right after import
import time

# Function to read and parse the YAML file
def read_yaml(path):
    with open(path, 'r') as file:
        try:
            return yaml.safe_load(file)
        except yaml.YAMLError as exc:
            logging.error(exc)
            sys.exit(1)

# Function to run a benchmark
def run_benchmark(script_path, times, warmup_times):
    # not used because we are using make commands, not shell scripts.
    #if not os.path.isfile(script_path):
    #    logging.error(f"Benchmark script {script_path} does not exist.")
    #    sys.exit(1)

    if warmup_times > 0:
        logging.info(f"Doing {warmup_times} warm-up runs of {script_path}.")

    for i in range(warmup_times):
        logging.debug(f"Warm-up run {i+1}/{warmup_times}")
        try:
            start_time = time.time()
            subprocess.run(script_path, shell=True, check=True)
            end_time = time.time()
            runtime = end_time - start_time
            logging.debug(f"Warm-up run for {script_path} took {runtime:.2f}s")
        except subprocess.CalledProcessError as e:
            logging.error(f"Running benchmark script {script_path} failed with: {e}")
            sys.exit(1)

    logging.info(f"Running {script_path} {times} times.")
    runtimes = []
    for i in range(times):
        logging.debug(f"Run {i+1}/{times}")
        try:
            start_time = time.time()
            subprocess.run(script_path, shell=True, check=True)
            end_time = time.time()
            runtime = end_time - start_time
            runtimes.append(runtime)
            logging.debug(f"Run for {script_path} took {runtime:.2f}s")
        except subprocess.CalledProcessError as e:
            logging.error(f"Running benchmark script {script_path} failed with: {e}")
            sys.exit(1)
    return runtimes

# Function to calculate the coefficient of variation
def coefficient_variation(data):
    return np.std(data) / np.mean(data) * 100

# Calculates the final score using the weighted geometric mean
def geometric_mean(scores):
    return np.prod([score**weight for _, score, weight in scores]) ** (1.0 / sum(weight for _, _, weight in scores))

def run_benchmarks(benchmarks, repeat, warmup, args):
    runtimes = {}
    for benchmark in benchmarks:
        name = benchmark['name']
        script_path = benchmark['runscript']
        owd = os.getcwd()
        os.chdir('../tasks/'+name)
        if args.no_prescript: # default is true, if no-prescript is set then this becomes false.
            print(" Running preparation steps before benchmarking: "+name)
            subprocess.run(benchmark['prescript'], shell=True, check=True)
        print(' Running benchmark: '+name)
        runtimes[name] = run_benchmark(script_path, repeat, warmup)
        mean = round(np.mean(runtimes[name]), 2)
        cv = round(coefficient_variation(runtimes[name]), 2)
        logging.info(f"{name} ran with an average runtime of {mean} and a coefficient of variation of {cv}")
        os.chdir(owd)
    return runtimes

def compute_scores(benchmark_runtimes, reference_system, cv_threshold):
    scores = []
    run_valid = True

    for name, runtimes in benchmark_runtimes.items():
        cv = coefficient_variation(runtimes)

        if cv > cv_threshold:
            logging.error(f"Benchmark {name} variability exceeded the threshold: {cv:.2f} > {cv_threshold:.2f}")
            logging.info("Marking run as invalid.")
            run_valid = False
            continue

        if name in reference_system and 'runtime' in reference_system[name]:
            reference_runtime = reference_system[name]['runtime']
            weight = reference_system[name].get('weight', 1)
            score = reference_runtime / np.mean(runtimes)
            scores.append((name, score, weight))
        else:
            logging.error(f"No reference runtime found for benchmark {name}.")
            logging.info("Marking run as invalid.")
            run_valid = False
            continue

    return scores, run_valid

def benchmark_scores(args):
    config = read_yaml(args.yaml_file)
    
    global_settings = config.get('globals', {})
    repeat = global_settings.get('repeat', 5)
    warmup = global_settings.get('warmup', 0)
    logging.debug(f"Each benchmark will be run {repeat} times.")
    cv_threshold = global_settings.get('cv_threshold', 5)
    logging.debug(f"Maximum threshold for coefficient of variation set to {cv_threshold:.2f}.")

    benchmark_runtimes = run_benchmarks(config['benchmarks'], repeat, warmup, args)
    scores, run_valid = compute_scores(benchmark_runtimes, config['reference_system'], cv_threshold)

    return scores, run_valid

# Main function
def main():
    # Set up logging
    logging.basicConfig(filename='STARS_logfile.txt')
    logging.basicConfig(level=logging.INFO)
    log = logging.getLogger()

    # Define the command-line arguments
    parser = argparse.ArgumentParser(description="SRC Network Representative Performance Score")
    parser.add_argument('-d', '--debug', action='store_true', help='Increase log level to debug.')
    parser.add_argument('--dry-run', action='store_true', help='Do a dry run. Does not actually run any benchmarks.')
    parser.add_argument('--runtimes-only', '--reference-system', action='store_true', help='Run benchmarks and report runtimes only, no scores. Outputs reference system yaml')
    parser.add_argument('yaml_file', help='Path to the YAML file with benchmark definitions.')
    parser.add_argument('--no-prescript', action='store_false', help='Disable prep steps like image-pull and data download')
    args = parser.parse_args()

    # Update log level if debug flag is set
    if args.debug:
        log.setLevel(logging.DEBUG)

    # Read configuration from YAML
    config = read_yaml(args.yaml_file)

    if args.runtimes_only:
        # Run benchmarks and report runtimes without computing scores
        global_settings = config.get('globals', {})
        repeat = global_settings.get('repeat', 5)
        warmup = global_settings.get('warmup', 0)
        benchmark_runtimes = run_benchmarks(config['benchmarks'], repeat, warmup, args)

        # Prepare runtimes in the format of 'reference_system' for YAML output
        reference_system_runtimes = {
            name: {'runtime': round(float(np.mean(runtimes)), 2)} for name, runtimes in benchmark_runtimes.items()
        }
        print(yaml.dump({'reference_system': reference_system_runtimes}))
    else:
        scores, run_valid = benchmark_scores(args)

        if not scores:
            logging.error("No valid benchmarks were run.")
            return

        # Print individual benchmark scores
        for name, score, weight in scores:
            logging.info(f"Benchmark {name}: Score {score:.2f}")

        # Calculate the weighted geometric mean of all scores
        final_score = geometric_mean(scores)
        logging.info(f"Final Score: {final_score:.2f}")

        if not run_valid:
            logging.error("Run is invalid due to a high coefficient of variation in one or more benchmarks.")

if __name__ == "__main__":
    main()
